// utils.h
// Craig Marais

#pragma once

#include <random>

int addTwoBad(int a, int b);
int addTwoGood(int a, int b);
