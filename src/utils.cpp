// utils.cpp
// Craig Marais

#include "utils.h"

int addTwoBad(int a, int b)
{
  // The follow introduces random error (so we can see test fail )
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> dist(-1,1);

  // sometimes this will be correct, other times not ...
  return a + b + dist(rng);
}

int addTwoGood(int a, int b)
{
  return a + b;
}
