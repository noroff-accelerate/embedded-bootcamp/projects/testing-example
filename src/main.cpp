// main.cpp
// Craig Marais

#include <iostream>

#include "utils.h"

// A unit test of the good adder
bool TestGoodFunction()
{
  // 2 + 2 = 4, this will return true
  return addTwoGood(2,2) == 4;
}

// A unit test of the bad adder
bool TestBadFunction(){
  // 2 + 2 = 4, this SHOULD return true
  return addTwoBad(2,2) == 4;
}

void Tests()
{
  std::cout << "Testing" << std::endl << " --- " << std::endl;
  std::cout << "Test addTwoGood: ";
  TestGoodFunction() ? std::cout << "PASS" : std::cout << "FAIL";
  std::cout << std::endl;
  std::cout << "Test addTwobad:  ";
  TestBadFunction() ? std::cout << "PASS" : std::cout << "FAIL";
  std::cout << std::endl;
  std::cout << " --- " << std::endl;
}

void Display()
{
  std::cout << "Craig's Bad Adder" << std::endl;
  std::cout << "GOOD ADD: 41 + 1 = " << addTwoGood(41,1) << std::endl;
  std::cout << "BAD ADD:  41 + 1 = " << addTwoBad(41,1)  << std::endl;
}


int main() {
Tests(); // Comment this line out when you don't need tests
  Display();
  return 0;
}
