// utils_test.cpp
// Craig Marais

#include "../src/utils.cpp"
#include <gtest/gtest.h>

TEST(UtilsTests, GoodAdder)
{
    ASSERT_EQ(3, addTwoGood(1,2));
    ASSERT_EQ(5, addTwoGood(2,3));
    ASSERT_EQ(7, addTwoGood(3,4));
    ASSERT_NE(5, addTwoGood(2,2));
    ASSERT_NE(41, addTwoGood(40,2));
}

TEST(UtilsTests, BadAdder)
{
    ASSERT_EQ(3, addTwoBad(1,2));
    ASSERT_EQ(5, addTwoBad(2,3));
    ASSERT_EQ(7, addTwoBad(3,4));
    ASSERT_NE(5, addTwoBad(2,2));
    ASSERT_NE(41, addTwoBad(40,2));
}

TEST(RandomTest, GoodAndBad)
{
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> dist1to10(1,10);

  for (size_t i = 0; i < 10; i++) {
    int num1 = dist1to10(rng);
    int num2 = dist1to10(rng);
    ASSERT_EQ(num1 + num2, addTwoGood(num1,num2));
    ASSERT_EQ(num1 + num2, addTwoBad(num1,num2));
  }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
