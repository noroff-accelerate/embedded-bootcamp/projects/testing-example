# Testing Example

This is an example project that uses
- [GoogleTest](https://github.com/google/googletest)
- [CTest](https://cmake.org/cmake/help/v2.8.8/ctest.html)

## Requirements

### CMake

```bash
sudo apt install cmake
```

### GoogleTest

Install GoogleTest

**Linux**:
```bash
sudo apt-get install libgtest-dev
```

**Compile GoogleTest**:
```
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make

# copy or symlink libgtest.a and libgtest_main.a to your /usr/lib folder
sudo cp *.a /usr/lib
```

## Usage

```bash
mkdir build && cd build
cmake ../

# Build
make

# Test
make test

# Run
./main
```

## Maintainers

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

## License

---

Copyright 2021, Craig Marais
